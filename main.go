package main

import (
	"net/http"
	"codinglife/authentication"
	"log"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/login", authentication.Login)
	mux.HandleFunc("/validate", authentication.ValidateToken)

	log.Println("Escuchando en puesto :8080")
	http.ListenAndServe(":8080", mux)
}